# Grupo de estudos de ReactJS da Comunidade DevOpsPBS

Ambiente Docker para o NLW Trilha ReactJS

## Criar um ambiente de desenvolvimento ReactJS com Docker

Com o git instalado e devidamente configurado rodar:

```console
git clone https://gitlab.com/devopspbs/front-end/reactjs-docker-compose.git
cd reactjs-docker-compose
```

Com docker e docker-compose devidamente instalados e configurados. 
Criar um novo projeto usando Create React App:

```console
docker-compose run react-app npx create-react-app .
```

Com docker e docker-compose devidamente instalados e configurados. 
Criar um novo projeto usando Create Next App:

```console
docker-compose run react-app npx create-next-app .
```

Em seguida inicie o ambiente:
```console
docker-compose up
```


Se você estiver rodando Docker diretamente sobre Linux é necessário ajustar as permissões. Lembre-se de repetir o comando abaixo sempre que estiver problemas para acessar os arquivos diretamente no Docker host.

```console
sudo chown -R $USER:$USER .
```

## Referências


## License

GPLv3 or later.